// import the Task model in our controllers, so that our controllers or controller functions may have access to our Task model
const Task = require("../models/Task");

// Create task
// end goal: create task without duplicate names
module.exports.createTaskController = (req, res) => {
	// checking captured data from request body
	console.log(req.body);
	// mongoDB: db.tasks.findOne({name: "nameOfTask"})
	// .then(anonymous function)
	// .then = promise (becomes asynchronous)
	Task.findOne({name: req.body.name}).then(result => {
		// checking the captured data from .then()
		console.log(result);
		// expected output:
			/*	{
					_ObjectId: 0456f41a54as4f1ds32h1r5,
					name: "nameOfTask"
					status: "pending"
				}
			*/
		if (result !== null && result.name === req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})
			// made into mutli line for readability
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

// Retrieve ALL Tasks

module.exports.getAllTasksController = (req, res) => {
	// similar: db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// Retrieve single task
module.exports.getSingleTaskController = (req, res) => {
	console.log(req.params);
	// similar: db.tasks.findOne({_id: "id"})
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// Updating task status
module.exports.updateTaskStatusController = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);
	let updates = {
		status : req.body.status
	};
	// findByIdAndUpdate()
		// 3 arguments
			// a. where will you get the id? req.params.id
			// b. what is the update? updates variable
			// c. optional: {new: true} - return the updated version of the document we are updating
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err));
};