const mongoose = require('mongoose');

// Mongoose Schema
	// Before we can create documents from our api to save into our database, we first have to determine the structure of the documents to be written in the database

	// A schema is a blueprint for our document

	// Schema() is a constructor from mongoose that will allow us to create a new schema object
const taskSchema = new mongoose.Schema({

	name: String,

	status: String
});

// Mongoose Model
/*
	Models are used to connect your api to the corresponding collection in your database. It is a representation of the Task document.

	Models used schema to create objects that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized
	
	mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

*/
module.exports = mongoose.model("Task", taskSchema);

// module.exports will allows us to export files/functions and be able to import/ require them in another file within our application

// Export the model into other files that may require it