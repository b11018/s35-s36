const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

console.log(userControllers);

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

// ACTIVITY 2
// update single username route
router.put('/:id', userControllers.updateUsernameController);
// get single user route
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;